package Divide_y_Venceras;

public class Cifrado {

    public static void main(String[] arg) {
        long x, mensajeCifrado, p, q;
        long z, e, invers;
        String mensaje = "hala";
        p = 997;
        q = 991;
  
        // System.out.println(Integer.toString(j, 2));
        //transformo el mensaje en numero para poder hacerCalculos
        z = generarClave(p, q);
                System.out.println(z);
        System.out.println(obtenerExponenteMasGrande(z));
        invers = generarClave(p - 1, q - 1);
        x = transformarTexto(mensaje,z);
        System.out.println(x);
        e = obtenerExponente(z);
        mensajeCifrado = exp(x, e, z);
        long d = inverso(e,invers);
        obtenerTexto(exp(mensajeCifrado, d, z));
        //mensajeCifrado = exp(x, e, z);
        //System.out.println(mensajeCifrado);
        //desencriptar(mensajeCifrado, z, e, mapa);
    }

    public static String obtenerTexto(long mensaje) {
        String texto ="";
        while (mensaje > 0) {
            //System.out.println(mensaje % 27);
            texto = obtenerCaracter((int) (mensaje % 28)) + texto;
            mensaje = mensaje / 28;
        }
        System.out.println(texto);
        return texto;
    }

    public static long generarClave(long p, long q) {
        return p * q;
    }

    public static char obtenerCaracter(int valor) {
        int caracter;
        char res;
        caracter = valor + 96;
        res =(char) caracter;
        if (valor > 14) {
            caracter--;
            res =(char) caracter;
        } else if (valor == 23) {
            res = 'ñ';
        }
        return res;
    }

    public static int obtenerValorCaracter(char caracter) {
        int valor;
        valor = caracter - 96;
        if (valor > 14) {
            valor++;
        }
        if (caracter == 'ñ') {
            valor = 23;
        }
        return valor;

    }
    public static int obtenerExponenteMasGrande(long z){
        int res = 0,exp = 1;
        while(exp*28 < z){
            exp = exp*28;
            res++;
        }
        return res;
    }

    public static long transformarTexto(String mensaje,long z) {
        mensaje = mensaje.toLowerCase();
        int j = 0, exponente = mensaje.length() - 1;
        for (int i = 0; i < mensaje.length(); i++) {
            
            j = (int) (j + (obtenerValorCaracter(mensaje.charAt(i)) * Math.pow(28, exponente)));
            exponente--;
        }
        return j;
    }

    public static long obtenerExponente(long z) {
        return 101;
    }
    
    public static long inverso(long a, long m) {
        long x=0;
        int b =0;
        while(b<m && x!=1){
            x = (a * b) % m;
            b++;
        }
        /*for (int b = 0; b < m; b++) {
            x = (a * b) % m;
            if (x == 1) {
                System.out.println(b);
            }*/
        b--;
        return b;
    }

    
    public static long transformarMensaje(String mensaje, BidirectionalMap mapa) {
        int longitud = mensaje.length();
        mensaje = mensaje.toLowerCase();
        String aux = "";
        for (int i = 0; i < longitud; i++) {
            aux = aux + mapa.get(mensaje.charAt(i));
        }
        return Long.parseLong(aux);
    }

    public static long exp(long a, long n, long z) {
        long i = n;
        long r = 1;
        long x = a % z;
        while (i > 0) {
            if (i % 2 != 0) {
                r = (r * x) % z;
            }
            x = (x * x) % z;
            i = i / 2;
        }
        return r;
    }

}
