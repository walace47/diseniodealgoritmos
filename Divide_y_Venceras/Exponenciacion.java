/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Divide_y_Venceras;

/**
 *
 * @author Joan
 */
public class Exponenciacion {
    public static void main(String[] arg){
        System.out.println(exp(2,6));
        System.out.println(exp(2,7));
        System.out.println(exp(2,8));
        System.out.println(exp(2,9));
        System.out.println(exp(2,100));
        System.out.println(Math.pow(2, 100));
    }
    
    
    
    public static double exp(double a, int n){
        int i = n;
        double r = 1;
        while(i>0){
            if (i%2 != 0){
                r = r*a;
            }
            a = a*a;
            i = i / 2;
        }
        return r;
    }
}
