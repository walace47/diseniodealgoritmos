/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package HashDeLetras;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.InputMismatchException;
import java.util.LinkedList;
import java.util.Scanner;

/**
 *
 * @author joan
 */
public class ListadoDePalabras {

    private HashMap<String, Nodo> hash;

    public ListadoDePalabras() {
        this.hash = new HashMap<>();
    }
    

    private void insertarPalabra(String cadena) {
        Nodo nodo;
        //Veo si esta la ocurrencia de esa palabra en el hash
        nodo = hash.get(cadena);
        if (nodo != null) {
            nodo.setContador();
        } else {
            nodo = new Nodo(cadena);
            hash.put(nodo.getPalabra(), nodo);
        }

    }

    private static LinkedList<String> cortarPalabra(String cadena) {
        //Este metodo corta la cadena y pone cada palabra en un arreglo
        int longitud;
        LinkedList<String> lista = null;
        String[] palabras;
        if (cadena != null) {
            palabras = cadena.split(" ");
            //System.out.println("longitud " + palabras.length);
            lista = new LinkedList<>(Arrays.asList(palabras));

        }

        return lista;
    }

    public Boolean insertarTexto(String cadena) {
        // este es el modulo que maneja los otros dos modulos de division e insercion
        boolean res = false;
        LinkedList<String> lista = cortarPalabra(cadena);
        if (lista != null) {
            for (int i = 0; i < lista.size(); i++) {
                insertarPalabra(lista.get(i));
            }
            lista.clear();
        }
        return res;
    }
    public void limpiar(){
        this.hash.clear();
    }
    public String toString(){
        String retorno = "";
        if (!hash.isEmpty()){
            retorno = hash.toString();
        }
        return retorno;
    }

   

}
