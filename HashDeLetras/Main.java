/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package HashDeLetras;

import java.util.InputMismatchException;
import java.util.Scanner;

/**
 *
 * @author joan
 */
public class Main {

    private static void menu() {
        System.out.println("\033[32m1. Insertar Palabra");
        System.out.println("\033[32m2. Ver ocurrencias de cada palabra");
        System.out.println("\033[32m3. Borrar lista");
        System.out.println("\033[32m4. Salir");
    }

    public static void main(String arg[]) {
        ListadoDePalabras list = new ListadoDePalabras();
        Scanner sn = new Scanner(System.in);
        Scanner sn2 = new Scanner(System.in);
        boolean salir = false;
        int opcion; //Guardaremos la opcion del usuario
        while (!salir) {
            menu();
            try {
                opcion = sn.nextInt();
                switch (opcion) {
                    case 1:
                        System.out.println("Insertar texto");
                        String cadena = sn2.nextLine();
                        list.insertarTexto(cadena);
                        break;
                    case 2:
                        System.out.println(list.toString());
                        break;
                    case 3:
                        list.limpiar();
                        break;
                    case 4:
                        salir = true;
                        break;
                    default:
                        System.out.println("Solo números entre 1 y 3");
                }
            } catch (InputMismatchException e) {
                System.err.println("Ingresa un entero porfavor");
                sn.nextLine();

            }

        }
    }

}
