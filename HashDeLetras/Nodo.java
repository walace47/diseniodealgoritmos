/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package HashDeLetras;

import java.util.Objects;

/**
 *
 * @author joan
 */
public class Nodo {
    private String palabra;
    private int contador;

    public Nodo(String palabra) {
        this.palabra = palabra;
        contador =1;
    }

    public String getPalabra() {
        return palabra;
    }

    public void setPalabra(String palabra) {
        this.palabra = palabra;
    }

    public int getContador() {
        return contador;
    }

    public void setContador() {
        this.contador++;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 37 * hash + Objects.hashCode(this.palabra);
        return hash;
    }
    
    public String toString(){
        String res;
        if (this.contador ==1)
            res = "Aparecio "+ this.contador + " vez\n";
        else
            res = "Aparecio "+ this.contador + " veces\n";
        return res;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Nodo other = (Nodo) obj;
        if (!Objects.equals(this.palabra, other.palabra)) {
            return false;
        }
        return true;
    }
    
}
