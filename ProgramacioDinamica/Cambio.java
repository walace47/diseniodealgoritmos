/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ProgramacioDinamica;

/**
 *
 * @author Joan
 */
public class Cambio {
    public static String realizarCambio(int cantidad, int[] monedas) {
        String respuesta;
        int[][] matrizDeCambio = calcularMonedas(cantidad, monedas);
        int[] seleccion = seleccionarMonedas(cantidad, monedas, matrizDeCambio);
        respuesta = "El mejor cambio para una cantidad de " + cantidad + " es:";
        for (int i = 0; i < seleccion.length; i++){
            respuesta = respuesta +"\nmonedas de "+ monedas[i]+" = "+seleccion[i];
        }
        return respuesta;
    }

    private static int[][] calcularMonedas(int cantidad, int[] monedas) {
        int[][] matrizCambio = new int[monedas.length + 1][cantidad + 1];
        for (int i = 0; i < monedas.length; i++) {
            matrizCambio[i][0] = 0;
        }
        for (int j = 1; j <= cantidad; j++) {
            matrizCambio[0][j] = 99;
        }
        for (int i = 1; i <= monedas.length; i++) {
            for (int j = 1; j <= cantidad; j++) {
                if (j < monedas[i - 1]) {
                    matrizCambio[i][j] = matrizCambio[i - 1][j];
                } else {
                    //int minimo = 0;
                    if(matrizCambio[i - 1][j] < matrizCambio[i][j - monedas[i - 1]] + 1){
                        matrizCambio[i][j] = matrizCambio[i - 1][j];
                    }else{
                        matrizCambio[i][j] = matrizCambio[i][j - monedas[i - 1]] + 1;
                    }
                  
                }
            }
        }

        return matrizCambio;
    }

    private static int[] seleccionarMonedas(int c, int[] monedas, int[][] tabla) {
        int i, j;
        int[] seleccion = new int[monedas.length];
        for (i = 0; i < monedas.length; i++) {
            seleccion[i] = 0;
        }
        i = monedas.length;
        j = c;
        while (j > 0) {
            if (i > 1 && tabla[i][j] == tabla[i - 1][j]) {
                i--;
            } else {
                seleccion[i - 1]++;
                j = j - monedas[i - 1];
            }
        }

        return seleccion;
    } 

}
