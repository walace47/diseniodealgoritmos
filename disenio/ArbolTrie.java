/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package disenio;

import java.util.LinkedList;

/**
 *
 * @author jstacco
 */
public class ArbolTrie {

    NodoTrie root;

    public ArbolTrie() {
        root = new NodoTrie('\0', null);
    }

    public void insert(String word) {
        NodoTrie current = root;
        for (int i = 0; i < word.length(); i++) {
            char c = word.charAt(i);
            NodoTrie sub = current.getChild(c);
            if (sub != null) {
                current = sub;
            } else {
                current.addChild(new NodoTrie(c, current));
                current = current.getChild(c);
            }

        }
        current.setEnds(true);
    }

    public boolean search(String word) {
        boolean encontro = true;
        int i = 0;
        NodoTrie current = root;
        while (i < word.length() && encontro) {
            char c = word.charAt(i);
            current = current.getChild(c);
            if (current == null) {
                encontro = false;
            }
            i++;
        }
        return encontro;
    }

    private LinkedList<String> buscarListaSinonimos(String word) {
        boolean encontro = true;
        int i = 0;
        NodoTrie current = root;
        LinkedList lista = null;
        while (i < word.length() && encontro) {
            char c = word.charAt(i);
            current = current.getChild(c);
            if (current == null) {
                encontro = false;
            }
            i++;
        }
        if (encontro) {
            lista = current.getLista();
        }
        return lista;
    }

    public boolean agregarDiccionario(String palabra, String sinonimo) {
        LinkedList lista;
        boolean exito;
        lista = buscarListaSinonimos(palabra);
        if (lista != null) {
            lista.add(sinonimo);
            exito = true;
        } else {
            exito = false;
        }
        return exito;
    }

    public LinkedList obtenerSinonimos(String palabra) {
        LinkedList lista= buscarListaSinonimos(palabra);
        if (lista == null){
            lista = new LinkedList();
        }
        return lista;
    }

    public LinkedList buscarPalabras(){
       NodoTrie current = root;
       LinkedList lista = new LinkedList();
       recorrer("",lista,current);
       return lista;
    }
    private void recorrer(String palabra, LinkedList lista, NodoTrie n) {
        if (n != null) {
            palabra = palabra + n.getContent();
            if(n.isEnds()){
                lista.add(palabra);
            }
            for (int i = 0; i< 26; i++){
                NodoTrie nodo;
                String nuevaPalabra = ""+palabra;
                nodo = n.getChild(i);
                recorrer(nuevaPalabra,lista,nodo);
            }
        }
    }

}
