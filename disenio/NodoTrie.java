/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package disenio;

import java.util.LinkedList;

/**
 *
 * @author jstacco
 */
public class NodoTrie {
    private char content;
    private boolean ends = false;
    private NodoTrie parent;
    private NodoTrie[] child = new NodoTrie[26];
    private LinkedList<String> lista;
 
    public NodoTrie(char c, NodoTrie pi)
    {
        parent = pi;
        content = c;
    }
 
    public NodoTrie getChild(char c) {
        return child[c-'a'];
    }
    public NodoTrie getChild(int c) {
        return child[c];
    }
 
    public NodoTrie addChild(NodoTrie node) { 
        child[node.content - 'a'] = node;
        return node;
    }

    public char getContent() {
        return content;
    }

    public void setContent(char content) {
        this.content = content;
    }

    public boolean isEnds() {
        return ends;
    }

    public void setEnds(boolean ends) {
        if (ends && lista == null ){
            lista = new LinkedList();
        }
        this.ends = ends;
    }

    public NodoTrie getParent() {
        return parent;
    }

    public void setParent(NodoTrie parent) {
        this.parent = parent;
    }

    public NodoTrie[] getChild() {
        return child;
    }

    public void setChild(NodoTrie[] child) {
        this.child = child;
    }

    public LinkedList getLista() {
        return lista;
    }


    
}
